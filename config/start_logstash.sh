#!/bin/bash
#chkconfig: 2345 80 90
#description: scipt for start Logstash manunaly or auto
start_model=$1
if [ "$start_model" == "" ]; then
    start_model="now"
fi
current_dir=`cd $(dirname $0); pwd -P`
cur_datetime="`date "+%Y-%m-%d %H:%m:%S"`"

echo "${cur_datetime}: test auto run script" >> ${current_dir}/auto_log
echo "The current directory is '$current_dir'"
if [ "$start_model" == "" -o "$start_model" == "-help" ]; then
    echo "【Error】: Accept args is 'now' or 'schedule'.Accept commands As follows:"
    echo "    $0 now : run logstash now"
    echo "    $0 schedule : run logstash on schedule"
    echo "    $0 test : run test on schedule"
elif [ "$start_model" == "test" ]; then
    echo 'Running test ...'
    rm -rf logs/info_test
    $current_dir/../logstash-6.5.4/bin/logstash -f $current_dir/changhang_logstash_test.conf
else
	if [ "$start_model" == "now" ]; then
		rm -rf logs/info_now
		echo 'Running now ...'
        echo "${cur_datetime}: start now" >> ${current_dir}/logs/start_now_log
		$current_dir/../logstash-6.5.4/bin/logstash -f $current_dir/changhang_logstash_now.conf
	elif [ "$start_model" == "schedule" ]; then
        echo 'Running schedule ...'
        echo "${cur_datetime}: start schedule" >> ${current_dir}/logs/start_schedule_log
		rm -rf logs/info_schedule
		$current_dir/../logstash-6.5.4/bin/logstash -f $current_dir/changhang_logstash_schedule.conf
	else
		echo "【Error】: $start_model is Illegal Argment"
	fi
fi



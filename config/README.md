### Logstash小工具

#### 目的：封装Logstash

**本文件使用前，将文件夹至于`logstash`同一级目录，如：**

dir/

- config/ `本文件根目录`
- logstash-6.5.4  `logstash根目录`
  - bin
  - lib
  - logs
  - ...

#### 目录结构

config/

- start_logstash.sh
- changhang_logstash_now.conf
- changhang_logstash_schedule.conf
- changhang_logstash_test.conf
- ship/
  - ship.sql
  - ship_mapping_template.json
- accident/
- shipcompanies/
- ship_test/
- oracle_driver/
  - ojdbc6.jar
- logs/
  - info_now
  - start_now_log
  - start_schedule_log

#### 目录简介

##### `start_logstash.sh`

启动`Logstash`的脚本，**运行之前需要给予可执行权限`chmod +x start_logstash.sh`。**

`start_logstash.sh`脚本参数列表：

- `now`: 启动`Logstash`并加载配置文件`changhang_logstash_now.conf`；**例如执行`start_logstash.sh now`启动并加载配置文件`changhang_logstash_now.conf`。**

- `schedule`: 启动`Logstash`并加载配置文件`changhang_logstash_schedule.conf`。
- `-help`： 查看脚本参数说明。

##### Logstash启动配置文件

`*.conf`文件为`Logstash`配置文件，配置输入（input）、过滤（filter）、输出（output）。`changhang_logstash_now.conf`与`changhang_logstash_schedule.conf`和`changhang_logstash_test.conf`的配置内容大体相同。三者之间的区别是：

- changhang_logstash_now.conf： 用于立即执行数据同步，设置定时任务为立即启动。
- changhang_logstash_schedule.conf：用于设置定时任务，例如设置凌晨3点同步。
- changhang_logstash_test.conf：用于测试，比如测试定时任务是否有效。

##### ship文件夹（与accident、shipcompanies结构等同）

- ship.sql - 从数据源**读取**数据。
- ship_mapping_template.json - `Elasticsearch`的`Mapping`文件。`Logstash`启动时，会根据此文件在`Elasticsearch`创建`Mapping`。

> `Mapping`是`Elasticsearch`中的表结构。

##### logs文件夹

- info_now：Logstash用于记录最后一条数据的某个字段（如ID），一般增量同步的时候使用，在`*.conf`中自定义。
- start_now_log：记录`start_logstash.sh now`的时间。
- start_schedule_log：记录`start_logstash.sh schedule`的时间。

##### oracle_driver

用于存放`Oracle`驱动包。

##### register_start

用于配置开机启动，即开机（或重启）时自动启动`register_start`，之后由`register_start`启动`start_logstash.sh`脚本，每次启动时会将启动时间写入`logs/start_schedule_log`。使用步骤：

1. 在`register_start`尾行追加`start_logstah.sh schedule`（即启动脚本`start_logstash.sh schedule`）。
2. 将`register_start`拷贝到`linux`的`/etc/init.d/`目录下。
3. 执行`chkconfig --add register_start`，将`register_start`注册到开机启动。

#### CHANGLOG

2019-2-15 更新。